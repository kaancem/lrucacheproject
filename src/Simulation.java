import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Simulation {
	private static final int DEFAULT_ASSOCIAVITY = 1;
	private static final int DEFAULT_CACHE_SIZE = 512;
	private static final int DEFAULT_BLOCK_SIZE = 4;
	private static final String DEFAULT_ADDRESS_FILENAME = "addresses.txt";
	private static final String DEFAULT_LOG_FILENAME = null;
	private static final int DEFAULT_RESET_FREQUENCY = 100;
	
	private static final long MAX_ADDRESS = (long) (Math.pow(2.0, 32) - 1);

	private static Logger logger = Logger.getInstance();
	
	public static void main(String[] args) throws NullPointerException, IOException  {
		int associativity = DEFAULT_ASSOCIAVITY;
		int cacheSize = DEFAULT_CACHE_SIZE;
		int blockSize = DEFAULT_BLOCK_SIZE;
		String addressFilename = DEFAULT_ADDRESS_FILENAME;
		String logFilename = DEFAULT_LOG_FILENAME;
		int resetFrequency = DEFAULT_RESET_FREQUENCY;
		
		String inputAssociativity = getCmdArgument(args, "a");
		if(inputAssociativity != null)
			try{
				associativity = Integer.parseInt(inputAssociativity);
			} catch(NumberFormatException e){
				quitWithError("Cannot parse given associativity: " + inputAssociativity);
			}
			
		if(!(associativity >= 1 && associativity <= 8))
			quitWithError("Associativiy must be and integer between 1 and 8, given: " + associativity);
		
		
		String inputCacheSize = getCmdArgument(args, "s");
		if(inputCacheSize != null)
			try {
				cacheSize = Integer.parseInt(inputCacheSize);
			} catch(NumberFormatException e){
				quitWithError("Cannot parse given cache size: " + inputCacheSize);
			}
		
		String inputBlockSize = getCmdArgument(args, "b");
		if(inputBlockSize != null)
			try {
				blockSize = Integer.parseInt(inputBlockSize);
			} catch(NumberFormatException e){
				quitWithError("Cannot parse given block size: " + inputBlockSize);
			}
		
		String inputAddressFilename = getCmdArgument(args, "f");
		if(inputAddressFilename != null)
			addressFilename = inputAddressFilename;
		
		String inputLogFilename = getCmdArgument(args, "d");
		if(inputLogFilename != null)
			logFilename = inputLogFilename;
		
		String inputResetFrequency = getCmdArgument(args, "n");
		if(inputResetFrequency != null)
			try{
				resetFrequency = Integer.parseInt(inputResetFrequency);
			} catch(NumberFormatException e){
				quitWithError("Cannot parse given reset frequency: " + inputResetFrequency);
			}
		
		if(resetFrequency < 0)
			quitWithError("Reset frequenct must be a non-negative integer, given: " + resetFrequency);
		
		// Cache size must be divisible by block size.
		if (!(cacheSize % (blockSize * associativity) == 0)){
			quitWithError("Cache size: " + cacheSize + " is not divisible by blockSize * associativity: " + (blockSize * associativity));
		}
		
		// Verifying all the command line arguments.
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(addressFilename));
		} catch (FileNotFoundException e) {
			quitWithError("Address file '" + addressFilename + "' not found.");
		}

		// Each line in the address file corresponds to an address.
		// We read the whole file and make sure that its valid.
		// All integers must be non-negative.
		// Addresses read are put into a ArrayList to run simulation later on.
		int lineNumber = 0;
		ArrayList<Integer> addressContainer=new ArrayList<Integer>();
		while(true){
			String line = reader.readLine();
			if(line == null)
				break;
			
			lineNumber++;
			line = line.trim();
			if(line.equals(""))
				continue; // Pass empty lines
			
			int address = 0;
			try{
				address = Integer.parseInt(line.trim());
			} catch(NumberFormatException e){
				reader.close();
				quitWithError("Non-integer value '" + line + "' found in line " + lineNumber);
			}
			
			if(address < 0){
				reader.close();
				quitWithError("A negative integer value '" + address + "' found in line " + lineNumber);
			}
			
			if(address > MAX_ADDRESS)
				quitWithError("A value overflowing 32 bit address range found: '" + address + "' in line " + lineNumber);

			addressContainer.add(address); // Finally add if address is valid.
		}
		
		// When we're done with verifying all arguments we can set the log filename.
		if(logFilename != null)
			logger.setFilename(logFilename);
		
		Cell.setResetFrequency(resetFrequency);
		Cache cache = new Cache(cacheSize, associativity, blockSize);
		
		logger.info("Starting cache simulation...");
		for(int i=0; i < addressContainer.size(); i++){
			int address = addressContainer.get(i);
			if((i+1) % 5 == 0)
				cache.write(address);
			else
				cache.get(address);
		}
		
		logger.info("Simulation finished.");
		logger.info("Results:");
		logger.info("Number of read misses: " + cache.getReadMissCount());
		logger.info("Number of read hits: " + cache.getReadHitCount());
		logger.info("Number of write misses: " + cache.getWriteMissCount());
		logger.info("Number of write hits: " + cache.getWriteHitCount());
		
		logger.info("Bye.");
		logger.closeWriter();

	}
	
	/**
	 * Exits the program with logging a message.
	 * @param errorString is the error message to log.
	 */
	private static void quitWithError(String errorString){
		logger.error(errorString);
		logger.closeWriter();
		System.exit(0);
	}
	
	/**
	 * Extracts the command line argument from the arguments array. 
	 * @param args is the arguments array.
	 * @param identifier is the identifier used for specifying an argument.
	 * @return value corresponding to the identifier. If no such identifier exists returns null.
	 */
	private static String getCmdArgument(String[] args, String identifier){
		for(int i=0;i<args.length;i++){
			if(args[i].equals("-" + identifier)&&i<args.length-1){
				return args[i+1].trim();
			}
		}
		return null;
	}
}
