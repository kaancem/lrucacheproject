import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class Logger {
	public static final int DEBUG_LEVEL = 2;
	public static final int INFO_LEVEL = 1;
	public static final int ERROR_LEVEL = 0;
	
	private static final String DEBUG_TEXT = "DEBUG";
	private static final String INFO_TEXT = "INFO";
	private static final String ERROR_TEXT = "ERROR";
	
	private int	logLevel = INFO_LEVEL;
	private String filename = null;
	private BufferedWriter writer = null ;
	private static Logger instance = null;
	private Logger(){}

	public static Logger getInstance() {
		if(instance == null) {
			instance = new Logger();
		}

		return instance;
	}

	public void error(String logString){
		if(logLevel >= ERROR_LEVEL){
			log(logString, ERROR_TEXT);
		}
	}

	public void info(String logString){
		if(logLevel >= INFO_LEVEL){
			log(logString, INFO_TEXT);
		}
		
	}

	public void debug(String logString){
		if(logLevel >= DEBUG_LEVEL){
			log(logString, DEBUG_TEXT);
		}
	}
	
	public void log(String logString, String logType){
		String outputString = logType + ":\t" + logString;
		System.out.println(outputString);
		if(filename != null)
			try{
				if(this.writer==null){
					this.writer = new BufferedWriter(new FileWriter(filename));
				}
				this.writer.write(outputString);
				this.writer.newLine();
			}
			catch(IOException e){
				System.out.println("Cannot log to file:");
				e.printStackTrace();
			}
	}
	
	public void setFilename(String filename) {
		closeWriter();
		this.filename = filename;
	}

	public void setlogLevel(int level){
		this.logLevel = level;
	}
	public void closeWriter(){
		if(writer!=null){
			try {
				this.writer.close();
				this.writer = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
